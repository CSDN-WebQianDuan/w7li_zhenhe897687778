var express = require('express');
var router = express.Router();
var PW=require("png-word");
var R = require("random-word");
var path = require("path");
var fs = require("fs");
var  test =''
/* GET home page. */
router.get('/', function(req, res, next) {
    if(!req.session.user){
        req.session.user = "";
    }
    //留言列表文件路径
    let msgFilePath = path.join(__dirname,"..","store_file","leavemessage.json");
    //读文件
    let msgFile=JSON.parse(fs.readFileSync(msgFilePath));
    let resData  = {
        title: '留言板',
        loginuser:req.session.user,
        leaveMsg:msgFile
    }
    res.render('index', resData);
});

/* 渲染登陆注册页面. */
router.get('/login', function(req, res, next) {
    res.render('login', { title: '登陆' });
});

var pw = new PW();
/* 获取验证码接口 */
router.get('/getVcode', function(req, res, next) {
    var r=new R("1234567890");
     req.session.vcode = r.random(4);

    //这段代码获取到的验证码写入到session在其他接口的session里无法获取到
    //pw.createReadStream(req.session.vcode).pipe(res);

    //解决方案返回图片地址用fetac请求
    let fileName= "pngword_"+new Date().getTime()+".png"
    let filePath = path.join(__dirname,"..","public","images",fileName);
    let readStream = pw.createReadStream(req.session.vcode).pipe(fs.createWriteStream(filePath));
    res.send({code:200,url:"http://localhost:8081/images/"+fileName});
});

/* 登陆接口. */
router.post('/login', function(req, res, next) {
    let loginmsg = {code:400,msg:"登陆失败"}
    if(req.session.vcode!==req.body.verificationcode){
        loginmsg = {code:400,msg:"验证码输入错误"}
        res.send(loginmsg);
        return
    }
    //用户列表文件路径
    let userFilePath = path.join(__dirname,"..","store_file","user.json");
    //读文件
    let userFile=JSON.parse(fs.readFileSync(userFilePath));
    for (let item of userFile){
        if(item.username === req.body.username){
            if(item.password!==req.body.password){
                loginmsg = {code:400,msg:"密码输入错误"}

            }else {
                req.session.user = req.body.username
                loginmsg = {code:200,msg:"登陆成功"}
            }
            break
        }else {
            loginmsg = {code:400,msg:"用户名不存在"}
        }

    }
    res.send(loginmsg);
});
/* GET 退出登陆 */
router.get('/logout', function(req, res, next) {
    req.session.user = "";
    res.send({code:200,msg:"退出成功"})
});

/* 注册接口. */
router.post('/reguser', function(req, res, next) {
    let returnRes = {code:200,msg:"注册成功"};
    if(!(req.session.vcode==req.body.verificationcode)){
        returnRes = {code:400,msg:"验证码输入错误"};
    }else {
        //注册用户列表文件路径
        let userFilePath = path.join(__dirname,"..","store_file","user.json");
        //读文件
        let userFile=JSON.parse(fs.readFileSync(userFilePath));
        //合并用户
        userFile.push({username:req.body.username,password:req.body.password});
        //写入文件
        fs.writeFileSync(userFilePath, JSON.stringify(userFile));
    }
    res.send(returnRes);
});

/* 留言接口. */
router.post('/submsg', function(req, res, next) {
    if(!req.body.msgText){
        res.send({code:400,msg:"留言信息不能为空"})
        return
    }
    if(!req.body.vCode){
        res.send({code:400,msg:"验证码不能为空"})
        return
    }
    if(!(req.session.vcode===req.body.vCode)){
        res.send({code:400,msg:"验证码输入错误"})
        return
    }
    //留言列表文件路径
    let msgFilePath = path.join(__dirname,"..","store_file","leavemessage.json");
    //读文件
    let msgFile=JSON.parse(fs.readFileSync(msgFilePath));
    //合并用户
    msgFile.push({user:req.session.user,msgText:req.body.msgText,time:getNowFormatDate()});
    //写入文件
    fs.writeFileSync(msgFilePath, JSON.stringify(msgFile));
    res.send({code:200,msg:"留言成功"});



    function getNowFormatDate() {
        var date = new Date();
        var seperator1 = "-";
        var year = date.getFullYear();
        var month = date.getMonth() + 1;
        var strDate = date.getDate();
        var hours = date.getHours();
        var minutes = date.getMinutes();
        var seconds = date.getSeconds();
        if (month >= 1 && month <= 9) {
            month = "0" + month;
        }
        if (strDate >= 0 && strDate <= 9) {
            strDate = "0" + strDate;
        }
        var currentdate = year + seperator1 + month + seperator1 + strDate+"  "+hours+":"+minutes+":"+seconds;
        return currentdate;
    }
});
module.exports = router;
